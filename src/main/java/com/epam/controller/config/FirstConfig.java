package com.epam.controller.config;

import com.epam.model.pojo.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("constant.properties")
@Import(SecondConfig.class)
public class FirstConfig {
    @Value("${beanB}")
    private String nameB;
    @Value("${beanBValue}")
    private int valueB;

    @DependsOn(value = "beanD")
    @Bean(value="beanB",initMethod = "init",destroyMethod = "destroy")
    public BeanB createBeanB() {
        return new BeanB();
    }

    @Value("${beanC}")
    private String nameC;
    @Value("${beanCValue}")
    private int valueC;

    @DependsOn(value = "beanB")
    @Bean(value = "beanC",initMethod = "init",destroyMethod = "destroy")
    public BeanC createBeanC() {
        return new BeanC();
    }

    @Value("${beanD}")
    private String nameD;
    @Value("${beanDValue}")
    private int valueD;

    @Bean(value = "beanD",initMethod = "init",destroyMethod = "destroy")
    public BeanD createBeanD() {
        return new BeanD();
    }

    @Bean("beanA1")
    public  BeanA createBeanA1()
    {
        BeanA beanA =new BeanA();
        beanA.setName(createBeanB().getName());
        beanA.setValue(createBeanC().getValue());
    return beanA;
    }
    @Bean("beanA2")
    public  BeanA createBeanA2()
    {
        BeanA beanA =new BeanA();
        beanA.setName(createBeanB().getName());
        beanA.setValue(createBeanD().getValue());
        return beanA;
    }
    @Bean("beanA3")
    public  BeanA createBeanA3()
    {
        BeanA beanA =new BeanA();
        beanA.setName(createBeanC().getName());
        beanA.setValue(createBeanD().getValue());
        return beanA;
    }
}
