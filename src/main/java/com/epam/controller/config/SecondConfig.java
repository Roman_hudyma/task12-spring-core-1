package com.epam.controller.config;

import com.epam.controller.BeanPostProcessorExecutor;
import com.epam.model.pojo.BeanE;
import com.epam.model.pojo.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class SecondConfig {
    @Bean
    @Lazy
    public BeanF initLazyBeanF()
    {
        return new BeanF();
    }
    @Bean
    public BeanE createBeanE()
    {
        return new BeanE();
    }
    @Bean
    public BeanPostProcessorExecutor validatorBeanPostProcessor() {
        return new BeanPostProcessorExecutor();
    }
}
