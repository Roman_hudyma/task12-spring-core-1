package com.epam.view;

import com.epam.controller.config.FirstConfig;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainView {
    public void run() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(FirstConfig.class);
        beanName(context);
        fullDescriptionBean(context);
    }

    private static void fullDescriptionBean(AnnotationConfigApplicationContext context) {
        for (String beanDefinitionName : context.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = context.getBeanDefinition(beanDefinitionName);
            System.out.println(beanDefinitionName + ": " + beanDefinition);
        }
    }

    private static void beanName(AnnotationConfigApplicationContext context) {
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            System.out.println(beanDefinitionName);
        }
    }

}
