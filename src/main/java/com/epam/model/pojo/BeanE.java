package com.epam.model.pojo;

import com.epam.model.BeanValidator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    private String name;
    private int value;
    private boolean valid;

    public BeanE() {
        System.out.println("BeanE created");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
    @PostConstruct
    public void initBean()
    {
        System.out.println("PostConstructor message");
    }
    @PreDestroy
    public  void destroyBean()
    {
        System.out.println("Destroy bean");
    }


    public void validate() {
        if (name != null && value > 0) {
            valid = true;
        }
    }
}
