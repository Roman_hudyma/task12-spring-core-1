package com.epam.model.pojo;

import com.epam.model.BeanValidator;

public class BeanF implements BeanValidator {
    private String name;
    private int value;
    private boolean valid;
    public BeanF() {
        System.out.println("BeanF created");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void validate() {
        if (name != null && value > 0) {
            valid = true;
        }
    }
}
