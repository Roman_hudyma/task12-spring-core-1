package com.epam.model.pojo;

import com.epam.model.BeanValidator;

public class BeanD implements BeanValidator {
    private String name;
    private int value;
    private boolean valid;
    public BeanD() {
        System.out.println("BeanD created");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void validate() {
        if (name != null && value > 0) {
            valid = true;
        }

    }
    private void init() {
        System.out.println("init " + toString());
    }

    private void destroy() {
        System.out.println("destroy " + toString());
    }
}
