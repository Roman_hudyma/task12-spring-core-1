package com.epam.model.pojo;

import com.epam.model.BeanValidator;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private String name;
    private int value;
    private boolean valid;

    public BeanA() {
        System.out.println("BeanA created");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public void validate() {
        if (name != null && value > 0) {
            valid = true;
        }
    }

    public void destroy() throws Exception {
        System.out.println("Init method after properties saved!");
    }

    public void afterPropertiesSet() throws Exception {
        System.out.println("Spring Container is destroy!");
    }
}
