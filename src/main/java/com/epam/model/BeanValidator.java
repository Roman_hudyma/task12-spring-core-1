package com.epam.model;

public interface BeanValidator {
   void validate();
}
